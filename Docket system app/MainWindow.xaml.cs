﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableDependency.Enums;
using TableDependency.EventArgs;
using TableDependency.SqlClient;
using System.Text.RegularExpressions;

namespace Docket_system_app
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //since Menu item is inherited from dockets,should be able to acess everything from there
        //personally i find it abit more mangagable to acesss them both seperatly
        Docket docketInstance = new Docket(); //istance of the docket class to acess methods
        menuItem menuItemInstance = new menuItem(); //istance of the MenuItem class to acess methods

        public MainWindow()
        {
            InitializeComponent();
            //setting up a timer here to refersh the list of dockets every 10 seconds
            //idea is to ensure that the list is up to date
            //in furture,I would rather have a better way to do this(something to check the database for new entrys,and then pull them out rather than checking constantly)
            DispatcherTimer timer = new DispatcherTimer(); //new timer
            timer.Tick += updatedocs_tick; //what we want to do when the timer hits the interval
            timer.Interval = new TimeSpan(0, 0, 10); //what is the invertval
            timer.Start(); //tell the timer to start
            List<Docket> dockets = docketInstance.getDockets(); //get a list of dockets from the database

            //loop though the dockets,add them to the listbox
            foreach (var item in dockets)
            {
                docketsLST.Items.Add("DocNum:" + item.docID.ToString() + "\n" + "TableNum" + item.docTableNum.ToString());
            }

            List<string> menuItems = menuItemInstance.getMenuItems(); //get the menu items from the database

            //loop though and add them to the listbox
            foreach (var item in menuItems)
            {
                menuitemsLST.Items.Add(item);
            }
        }

        //method to refresh the list of dockets using a timer
        private void updatedocs_tick(object sender, EventArgs e)
        {
            
            int selectedID = docketsLST.SelectedIndex;//istance of the docket class to acess methods
            docketsLST.Items.Clear(); //clear the list
            List<Docket> dockets = docketInstance.getDockets(); //get a new list of all the documets

            //add these to the listbox
            foreach (var item in dockets)
            {
                docketsLST.Items.Add("DocNum:" + item.docID.ToString() + "\n" + "TableNum" + item.docTableNum.ToString());
            }
            docketsLST.SelectedIndex = selectedID; //go back to whatever the user have clicked on

        }

        //method to fire whenever a user changes the selected docket
        //this will pull all the details of said docket onto the screen
        private void docketsLST_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MenuItemsLST.Items.Clear(); //firstly clear anything out of the Menu items list
            int selected = docketsLST.SelectedIndex +1; //+1 to account for database IDS starting at 1
            List<string> docketMenuItems = menuItemInstance.getDocketMenuItems(selected);//get a list of menu items tied to the docket
            Docket docketDetails = docketInstance.getDocketDetails(selected); //get the details of the docket

            //loop though and get add all the docket menu items to the listbox
            if (docketMenuItems != null) //check to make sure we didnt pull a null to avoid errors
            {
                foreach (var item in docketMenuItems)
                {
                    MenuItemsLST.Items.Add(item);
                }
            }

            if (docketDetails != null)//check to make sure we didnt pull a null
            {
                //then just add the info to the apporiate labels
                DocketSeverLBL.Content = "Sever:" + docketDetails.docketSever;
                DocketTimeLBL.Content = "Issued:" + docketDetails.docketAddedTime.ToShortTimeString();
                NotesTBX.Text = docketDetails.docketNotes;
                AlergysTBX.Text = docketDetails.docketAlergyNotes;
            }


        }

        //method to pull out details of any given item on the menu
        private void menuitemsLST_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //have to use a string since IDS wont match up to indexes on the list
            //EG the two items on a docket could be ids 12 and 5
            string selected = menuitemsLST.SelectedIndex.ToString();//first get what item we have selected
            
            menuItem menuitems = menuItemInstance.getMenuItemsDetails(selected); //then get the details off the database

            if (menuitems != null) //make sure we dont have a null
            {
                //then print this information to the apporiate labels
                menuItemPriceLBL.Content = "Price:€" + menuitems.menuItemPrice.ToString();
                menuItemDescLBL.Content = "Description:" + menuitems.menuItemDesc.ToString();
            }


        }

        //method to add an menu item to a docket
        private void addItemToOrder_Click(object sender, RoutedEventArgs e)
        {
            
               string selected = menuitemsLST.SelectedIndex.ToString();//get the slected item from the menu items list
               menuItemsOrderedLST.Items.Add(menuitemsLST.SelectedValue.ToString()); //add that item to the ordered list
               menuItem currentItem = menuItemInstance.getMenuItemsDetails(selected); //get the details of the menu item from the database from the database

               double itemPrice = currentItem.menuItemPrice; //get the current price of our item
               double currentPrince = Convert.ToDouble(totalPriceLBL.Content); //get the overall cost of the docket
               totalPriceLBL.Content = Math.Round(currentPrince + itemPrice); //then add these two together and apply this to the label


        }

        //unfinhsed method for adding a new docket to the database
        //so far this end of it is complete(not tested however)
        private void addDocket_Click(object sender, RoutedEventArgs e)
        {
            
            int itemsOnDockets = menuItemsOrderedLST.Items.Count;//get the number of items on a docket
            List<string> itemNames = new List<string>();//create an empty list to hold the names of items

            //loop though and get the items,add them to the list
            for (int i = 0; i < itemsOnDockets; i++)
            {
                itemNames.Add(menuItemsOrderedLST.Items.GetItemAt(i).ToString());
            }

            menuItemInstance.addDocketToDB(itemNames); //call the method to add these to the database
       
        }
    }
}
