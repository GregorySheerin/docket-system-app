﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableDependency.SqlClient;

namespace Docket_system_app
{
    class Docket
    {
        public int docID { get; set; } //id of the docket from db
        public string docTableNum { get; set; } //what table is the docket assigned to
        public string docketSever { get; set; } //what sever is the docket assigned to
        public string docketNotes { get; set; } //any extra notes on a docket,eg no mushrooms
        public string docketAlergyNotes { get; set; } //any alergys attached to the docket,eg if someone have a alergy to peanutes
        public DateTime docketAddedTime { get; set; } //what time was the docket added to the system

        //method to get a list of dockets out of the database
        //this will just get the ID and the Table it is assinged to
        //rest is down in a seperate method
        public List<Docket> getDockets()
        {
            DataTable dt = new DataTable();
            List<Docket> returnlst = new List<Docket>();
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-9S3H0FR\\SQLEXPRESS;Initial Catalog=DocketSystemDB;Integrated Security=True"))
            {
                SqlCommand cmd = new SqlCommand("getDockets", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(dt);
              
            }

            foreach (DataRow dr in dt.Rows)
            {
                returnlst.Add(
                    new Docket
                    { 
                    docID = Convert.ToInt32(dr["DocketID"]),
                    docTableNum = dr["TableName"].ToString(),

                    }
                    );
            }

            return returnlst;
        }

        //method to grab the details of a certain docket
        //use the id for this as we can do a very simple query on the database end
        public Docket getDocketDetails(int docketID)
        {
            DataTable dt = new DataTable();
            Docket returnDetails = new Docket();
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-9S3H0FR\\SQLEXPRESS;Initial Catalog=DocketSystemDB;Integrated Security=True"))
            {
                SqlCommand cmd = new SqlCommand("getDocketsDetails", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@eDocketID", SqlDbType.Int).Value = docketID;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(dt);

            }
            if (dt.Rows.Count != 0)
            {
                 returnDetails = new Docket
                {
                    docketSever = dt.Rows[0]["DocketSever"].ToString(),
                    docketNotes = dt.Rows[0]["DocketNotes"].ToString(),
                    docketAddedTime = (DateTime)dt.Rows[0]["DocketTime"],
                    docketAlergyNotes = dt.Rows[0]["DocketAlergyNotes"].ToString()
                };
            }

            return returnDetails;
        }
    }

    //inherited from the docket class,each docket will have menu items on it
    //in hinsight,these could of been kept seperate from eachother,leave any relationships to the database
    class menuItem:Docket
    {
        public int menuitemID { get; set; } //the id of a menu item in the database
        public string menuItemName { get; set; } //the name of the item
        public string menuItemDesc { get; set; } //the description of an item
        public double menuItemPrice { get; set; } //the items price
        public string menuItemAlergy { get; set; } //any alergy the item may contain

        //method to get all the menu items that would be on a docket
        public List<string> getDocketMenuItems(int docketID)
        {
            List<string> returnlst = new List<string>();
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-9S3H0FR\\SQLEXPRESS;Initial Catalog=DocketSystemDB;Integrated Security=True"))
            {               
                SqlCommand cmd = new SqlCommand("getMenuItemsForDocket", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@eDocketID", SqlDbType.Int).Value = docketID;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(dt);
            }

            foreach (DataRow dr in dt.Rows)
            {
                returnlst.Add(dr["MenuItemName"].ToString());     
            }

            return returnlst;
        }

        //method to return a list of All the menu items
        //since menus change on a regular basis,it would be better to have a menu,then items attached to a menu
        //but this would do if we assume the menu is static
        public List<string> getMenuItems()
        {
            List<string> returnlst = new List<string>();

            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-9S3H0FR\\SQLEXPRESS;Initial Catalog=DocketSystemDB;Integrated Security=True"))
            {
                SqlCommand cmd = new SqlCommand("getAllMenuItems", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(dt);
            }

            foreach (DataRow dr in dt.Rows)
            {
                returnlst.Add(dr["MenuItemName"].ToString());
            }
            return returnlst;
        }

        //method to get all the details from a menu item using its name
        public menuItem getMenuItemsDetails(string name)
        {
            menuItem returnItem =new menuItem();

            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-9S3H0FR\\SQLEXPRESS;Initial Catalog=DocketSystemDB;Integrated Security=True"))
            {
                SqlCommand cmd = new SqlCommand("getAllMenuItemsDetails", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@emenuItemName", SqlDbType.VarChar).Value = name;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(dt);
            }

            if (dt.Rows.Count != 0)
            {
                returnItem = new menuItem
                {

                    menuItemDesc = dt.Rows[0]["MenuItemDesc"].ToString(),
                    menuItemPrice =Convert.ToDouble( dt.Rows[0]["MenuItemPrice"].ToString())

                };
            }

          
            return returnItem;
        }

        //method to add a docket to the database
        //This method was to add a new docket to the database,however it is unfinsihed
        //since this reads we are trying to add a list into a sproc,it wont work(sql will have no idea what im sending it)
        //the list would need to be looped though,to add a set of names
        public void addDocketToDB(List<string> names)
        {
            menuItem returnItem = new menuItem();

            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection("Data Source=DESKTOP-9S3H0FR\\SQLEXPRESS;Initial Catalog=DocketSystemDB;Integrated Security=True"))
            {
                SqlCommand cmd = new SqlCommand("addDocket", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //main issue is here,cant thorw a list to sql,so this would need a loop to add a menu item 1 at a time 
                cmd.Parameters.Add("@emenuItemName", SqlDbType.VarChar).Value = names; 
            }
    
        }
    }
    }

    

   

